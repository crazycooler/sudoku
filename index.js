import React,{Component} from 'react'
import { AppRegistry } from 'react-native';
//import App from './App';

import { PersistGate } from 'redux-persist/es/integration/react'
import {Provider} from 'react-redux'
import {persistor,store} from './src/store'
import RootNavigator from './src/RootNavigator'
import RootTabs from './src/test/RootTabs'
import RootDrawer from './src/test/RootDrawer'
import { NavigationActions ,StackRouter} from 'react-navigation'

import Main from './src/Main'

import {connect} from 'react-redux'
import { addNavigationHelpers } from 'react-navigation'

export default class App extends Component {
	render(){
		return (
			<RootNavigator
				navigation={addNavigationHelpers({
	        dispatch: this.props.dispatch,
	        state: this.props.nav,
	      })}
			/>
		)
	}
}


const mapStateToProps = (state) => ({
	nav:state.nav
});

const AppWithNavigationState = connect(mapStateToProps)(App);

class Root extends Component{
	render(){
		return (
			<Provider store={store}>
				<PersistGate
					persistor={persistor}>
					<AppWithNavigationState/>
				</PersistGate>
			</Provider>
		);
	}
}

AppRegistry.registerComponent('sudoku', () => Root);
