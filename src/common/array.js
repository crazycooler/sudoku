/**
 * Created by coder on 2017/12/28.
 */
export const initialize2DArray = (w,h,val = null) =>
	Array(h).fill().map(
		() => Array(w).fill(val)
	);

export const initializeArrayWithValues = (n,value=0) =>
	Array(n).fill(value);