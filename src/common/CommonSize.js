/**
 * Created by coder on 2017/12/26.
 */
import {Dimensions} from 'react-native'

const deviceH = Dimensions.get('window').height;
const deviceW = Dimensions.get('window').width;

export const gridCellW = (deviceW-20)/9;
export const gridMarginLeft = (deviceW-gridCellW*9-2)/2;  //有两个中间一个单位的缝隙

export const keyboardH = gridCellW;
export const keyboardW5 = (deviceW-gridMarginLeft*2)/5;
export const keyboardW4 = (deviceW-gridMarginLeft*2)/4;
export const circleSize = gridCellW/8;

export const levelCellW = (deviceW-20)/5;