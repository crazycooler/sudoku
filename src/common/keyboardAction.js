/**
 * Created by coder on 2018/1/15.
 */

import {ACTION_KEYBOARD_SETTING} from '../store/action'


const getNumEnable = (cells,curItem) => {
	//设置键盘，那些数字是可以点的
	let numEnable = [true,true,true,true,true,true,true,true,true];

	cells.map((cell) => {
		if(!curItem.isStatic){
			if(cell.x === curItem.x ||
				cell.y === curItem.y ||
				(parseInt(cell.x/3) === parseInt(curItem.x/3) &&
				parseInt(cell.y/3) === parseInt(curItem.y/3))){

				if(cell.x === curItem.x && cell.y === curItem.y)
					return;

				if(cell.data > 0){
					numEnable[cell.data-1] = false;
				}
			}
		}
	});

	return numEnable;
};

export const changeCellSel = (curItem) => {

	return (dispatch,getState) => {
		const {cells} = getState();

		if(!curItem.isStatic){
			//设置键盘，那些数字是可以点的
			let numEnable = getNumEnable(cells,curItem);

			dispatch({
				type:ACTION_KEYBOARD_SETTING,
				payload:{
					numEnable,
					show:true,
					mark:curItem.isMark,
				}
			});

		} else {
			dispatch({
				type:ACTION_KEYBOARD_SETTING,
				payload:{
					show:false,
				}
			});
		}
	}

};