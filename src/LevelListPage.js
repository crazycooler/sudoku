/**
 * Created by coder on 2018/1/2.
 */
import React,{Component} from 'react'
import {ScrollView,View,Text,StyleSheet,TouchableNativeFeedback} from 'react-native'
import {levelCellW} from './common/CommonSize'
import Icon from 'react-native-vector-icons/FontAwesome'
import {connect} from 'react-redux'

const indexOfClass = ['c1','c2','c3','c4','c5'];
const indexOfTitle = ['入门','初级','中级','高级','骨灰级'];

class LevelListPage extends Component{
	static navigationOptions = ({navigation}) => {
		const title = indexOfTitle[navigation.state.params.classIndex];
		return {
			headerTitle:title,
			headerStyle:{
				height:50,
			},
		};
	};


	constructor(props){
		super(props);
	}

	renderLevelItem = (i) => {
		const {curLevel} = this.props.level;
		if(i<curLevel){
			return (<Text style={styles.levelButtonText}>{i+1}</Text>);
		} else if(i === curLevel){
			return (<Icon name="unlock-alt" style={[styles.levelButtonIcon,{color:'#ff9c1a'}]}/>);
		} else {
			return (<Icon name="lock" style={styles.levelButtonIcon}/>);
		}
	};

	renderLevelButton = (i) => {
		const {curLevel} = this.props.level;
		const {navigate} = this.props.navigation;
		const {classIndex} = this.props.navigation.state.params;

		if(i <= curLevel){
			return (
				<TouchableNativeFeedback
					key={i}
					onPress={() => {navigate('Main',{classIndex,level:i})}}
				>
					<View
						style={[styles.levelButton]}
					>
						{
							this.renderLevelItem(i)
						}
					</View>
				</TouchableNativeFeedback>
			);
		} else {
			return (
				<View
					key={i}
					style={[styles.levelButton]}
				>
					{
						this.renderLevelItem(i)
					}
				</View>
			);
		}
	};

	render(){
		const {totalLevel} = this.props.level;
		return (
			<ScrollView contentContainerStyle={{margin:10,flexDirection:'row',flexWrap:'wrap'}}>
				{
					Array(totalLevel).fill().map(
						(v,i) => {
							return this.renderLevelButton(i);
					})
				}
				<View style={{width:'100%',height:30}} />
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	levelButton:{
		width:levelCellW*0.8,
		height:levelCellW*0.8,
		margin:levelCellW*0.1,
		borderWidth:1,
		borderColor:'#5bc5ef',
		alignItems:'center',
		justifyContent:'center',
		borderRadius:levelCellW*0.1,
	},
	levelButtonText:{
		fontSize:levelCellW*0.4,
		color:'#ff9c1a',
	},
	levelButtonIcon:{
		fontSize:levelCellW*0.4,
	},
	unlockedColor:{
		borderColor:'#ff9c1a',
	},
	lockedColor:{
		borderColor:'#6b6b6e',
	}
});

const mapStateToProps = (state,ownProps) => {
	const {classIndex} = ownProps.navigation.state.params;
	return {
		level:state.level[indexOfClass[classIndex]]
	}
};


export default connect(mapStateToProps)(LevelListPage);