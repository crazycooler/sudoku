/**
 * Created by crazycooler on 2017/12/27.
 */
import initState from './state'
import {ACTION_SET_CELLS,
	ACTION_UPDATE_CELLS,
	ACTION_UPDATE_ALL_CELLS,
	ACTION_CELLS_RESET} from './action'
import {initializeArrayWithValues} from '../common/array'

export default (state = initState.cells,action) => {
	switch(action.type){
		case ACTION_SET_CELLS:
			const sudoData = action.payload.data.split('');
			return Array(81).fill().map((v,index) => {
				const data = parseInt(sudoData[index]);
				return {
					x:index%9,
					y:parseInt(index/9),
					data:data,
					isStatic:!(data === 0),
					isMark:false,
					numMark:initializeArrayWithValues(9,false),
				}
			});
		case ACTION_UPDATE_CELLS:
			const {x,y,cell,needRecreate} = action.payload;
			let varArr = state.slice();
			varArr[y*9+x] = needRecreate ? Object.assign({},cell) : cell;
			return varArr;
		case ACTION_UPDATE_ALL_CELLS:
			return action.payload;
		case ACTION_CELLS_RESET:
			return initState.cells;
		default:
			return state;
	}
}
