/**
 * Created by coder on 2018/1/3.
 */
import RootNavigator from '../RootNavigator'

const initialState = RootNavigator.router.getStateForAction(RootNavigator.router.getActionForPathAndParams('Home'));

const navReducer = (state = initialState,action) => {
	const nextState = RootNavigator.router.getStateForAction(action,state);
	return nextState || state;
};

export default navReducer;