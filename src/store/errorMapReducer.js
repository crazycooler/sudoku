/**
 * Created by coder on 2017/12/28.
 */
import initState from './state'
import {ACTION_UPDATE_ERROR_MAP,
	ACTION_ERROR_MAP_RESET} from './action'
import {initializeArrayWithValues} from '../common/array'

export default (state = initState.errorMap,action) => {
	switch (action.type){
		case ACTION_UPDATE_ERROR_MAP:
			return action.payload.errorMap;
		case ACTION_ERROR_MAP_RESET:
			return initializeArrayWithValues(81,false);
		default:
			return state;
	}
}