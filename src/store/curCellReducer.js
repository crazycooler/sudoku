/**
 * Created by crazycooler on 2017/12/27.
 */
import initState from './state'
import {ACTION_SET_CUR_CELL,
	ACTION_UPDATE_CUR_CELL,
	ACTION_CUR_CELL_RESET} from './action'
import {initializeArrayWithValues} from '../common/array'

export default (state = initState.curCell,action) => {
	switch (action.type) {
		case ACTION_SET_CUR_CELL:
			return action.payload.cell;
		case ACTION_UPDATE_CUR_CELL:
			const {value,isMark,numMark} = action.payload;
			if(typeof value === 'number'){
				if(isMark){
					return Object.assign({},state,{data:0,numMark:numMark,isMark:true});
				} else {
					return Object.assign({},state,{data:value,isMark:false});
				}
			}  else if(value === 'clear'){
				return Object.assign({},state,{data:0,isMark:false,numMark:initializeArrayWithValues(9,false)})
			} else if(value === 'undo'){

			} else if(value === 'redo'){

			}
			return state;
		case ACTION_CUR_CELL_RESET:
			return initState.curCell;
		default:
			return state;
	}
}
