/**
 * Created by coder on 2018/1/2.
 */
import initState from './state'
import {ACTION_SET_LEVEL,ACTION_SET_CUR_LEVEL,ACTION_FINISH_CUR_LEVEL} from './action'

const indexOfClass = ['c1','c2','c3','c4','c5'];

export default (state = initState.level,action) => {
	switch (action.type){
		case ACTION_SET_LEVEL:{
			const {classIndex,data} = action.payload;
			const className = indexOfClass[classIndex];
			return Object.assign({},state,{[className]:data});
		}
		case ACTION_SET_CUR_LEVEL:
			return Object.assign({},state,{curLevel:action.payload.curLevel});
		case ACTION_FINISH_CUR_LEVEL:{
			const {classIndex,level} = action.payload;
			const className = indexOfClass[classIndex];
			if(level === state[className].curLevel){
				return Object.assign({},state,{
					[className]:{
						curLevel:level+1,
						totalLevel:state[className].totalLevel
					},
					curLevel:{
						classIndex:-1,
						level:-1,
					}
				});
			} else {
				return Object.assign({},state,{curLevel:{classIndex:-1, level:-1,}});
			}
		}
		default:
			return state;
	}
}