/**
 * Created by coder on 2017/12/26.
 */
import {ACTION_KEYBOARD_FILTER,
	ACTION_KEYBOARD_NUM_ENABLE,
	ACTION_KEYBOARD_SHOW,
	ACTION_KEYBOARD_SETTING,
	ACTION_KEYBOARD_RESET} from './action'
import initState from './state'

export default (state = initState.keyboard,action) => {
	switch (action.type){
		case ACTION_KEYBOARD_FILTER:
			return Object.assign({},state,{filter:!state.filter});
		case ACTION_KEYBOARD_NUM_ENABLE:
			return Object.assign({},state,{numEnable:action.payload.numEnable});
		case ACTION_KEYBOARD_SHOW:
			return Object.assign({},state,{show:action.payload.show});
		case ACTION_KEYBOARD_SETTING:
			return Object.assign({},state,action.payload);
		case ACTION_KEYBOARD_RESET:
			return initState.keyboard;
		default:
			return state;
	}
}