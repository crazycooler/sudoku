/**
 * Created by coder on 2017/12/26.
 */
import {applyMiddleware,createStore,combineReducers,compose} from 'redux'
import thunk from 'redux-thunk'

import {createLogger} from 'redux-logger'
import { persistStore,persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import keyboardReducer from './keyboardReducer'
import curCellReducer from './curCellReducer'
import cellsReducer from './cellsReducer'
import errorMapReducer from './errorMapReducer'
import levelReducer from './levelReducer'
import navReducer from './navReducer'
import memoryReducer from './memoryReducer'

const config = {
	key: 'primary',
	storage
};

const middlewares = [thunk];
if(process.env.NODE_ENV == 'development'){
	let logger = createLogger({
		collapsed:true,
		duration:true,
	});
	middlewares.push(logger);
}

const rootReducer = persistCombineReducers(config,{
	keyboard:keyboardReducer,
	curCell:curCellReducer,
	cells:cellsReducer,
	errorMap:errorMapReducer,
	level:levelReducer,
	nav:navReducer,
	memory:memoryReducer,
});

export const store = createStore(
	rootReducer,
	undefined,
	compose(
		applyMiddleware(...middlewares),
	),
);

export const persistor = persistStore(store,null,() => {
	store.getState()
});

persistor.purge();


