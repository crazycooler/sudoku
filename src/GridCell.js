/**
 * Created by crazycooler on 2017/12/24.
 */
import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {View,Text,StyleSheet,TouchableWithoutFeedback} from 'react-native'

export default class GridCell extends Component{
	constructor(props){
		super(props);
	}

	cellPress = () => {
		const {item,onChangeState} = this.props;
		onChangeState(item);
	};

	renderMark = (cellStyle) => {
		const {style,size,item} = this.props;
		const contentSize = size - 6;
		return (
			<View style={[style,cellStyle,styles.markCellView]}>
				{item.numMark.map((value,index) => {
					return (
						<Text key={index}
							style={[styles.markCell,{width:contentSize/3,height:contentSize/3,fontSize:size/3*0.75}]}>
						{value ? (index+1) : null}
						</Text>
					)
				})}
			</View>
		);
	};

	renderNormal = (cellStyle) => {
		const {item,size,style} = this.props;
		const fontSize = size*0.9;

		return (
			<View style={[style,cellStyle]}>
				{
					item.data === 0 ?
						null:
						(<Text style={[{fontSize},item.isStatic ? null : styles.normalCell]}>{item.data}</Text>)
				}
			</View>
		);
	};

	render(){
		const {item,active,error} = this.props;

		let cellStyle = item.isStatic ? styles.staticCell : styles.emptyCell;
		if(active === 1){
			cellStyle = styles.activeCell;
		} else if(active === 2){
			cellStyle = styles.selectCell;
		}

		cellStyle = error ? styles.errorCell : cellStyle;

		return (
			<TouchableWithoutFeedback onPress={this.cellPress}>
				{
					!item.isStatic && item.data === 0 && item.isMark ?
						this.renderMark(cellStyle)
						:
						this.renderNormal(cellStyle)
				}
			</TouchableWithoutFeedback>
		);
	}
}


GridCell.propTypes = {
	size:PropTypes.number,
	item:PropTypes.object,
	onChangeState:PropTypes.func,
	active:PropTypes.number,  //0.nothing 1.不能相同的区域  2.相同的数字 3.当前选中
	error:PropTypes.bool,
};

const styles = StyleSheet.create({
	emptyCell:{

	},
	staticCell:{
		backgroundColor:'#bbb'
	},
	selectCell:{
		backgroundColor:'#ff9c1a'
	},
	activeCell:{
		backgroundColor:'#efdfff'
	},
	normalCell:{
		color:'#625eff'
	},
	markCell:{
		color:'#ff2a34'
	},
	errorCell:{
		backgroundColor:'#ff837d'
	},
	markCellView:{
		flexDirection:'row',
		flexWrap:'wrap',
		padding:1,
	}

});