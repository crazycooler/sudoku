/**
 * Created by crazycooler on 2017/12/24.
 */
import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {View,Dimensions,Text,StyleSheet} from 'react-native'
import GridCell from './GridCell'
import {gridCellW,gridMarginLeft} from './common/CommonSize'
import {connect} from 'react-redux'
import {ACTION_SET_CUR_CELL,ACTION_UPDATE_CELLS,ACTION_KEYBOARD_SETTING} from './store/action'
import {changeCellSel} from './common/keyboardAction'
import {addMemory} from './common/memoryAction'


class GridLayout extends Component{
	constructor(props){
		super(props);
	}

	componentDidMount(){

	}

	isCellTheSame = (orgItem,newItem) => {
		if(orgItem.isStatic)
			return true;

		if(orgItem.isMark !== newItem.isMark)
			return false;

		if(orgItem.isMark){
			for(let i=0;i<9;i++){
				if(orgItem.numMark[i] !== newItem.numMark[i])
					return false;
			}
			return true;
		} else {
			return orgItem.data === newItem.data;
		}
	};

	//选中的cell改变时
	onChangeCellState = (item) => {

		//curCell已经是上一个cell了，当前的cell将变成item
		const {curCell,cells} = this.props;
		if(curCell.x >= 0){
			//将当前修改的cell数据，写入到cells中
			const orgCell = cells[curCell.y * 9 + curCell.x];
			//不一样的才修改,一般mark的才需要这样操作，因为填数字的直接就写入了
			if(!this.isCellTheSame(orgCell,curCell)){
				this.props.updateCells(curCell);
				this.props.addMemory();
			}
		}

		//设置当前的Cell
		this.props.setCurCell(Object.assign({},item));

		//对键盘的影响
		this.props.changeCellSelOfKeyboard(item);

	};

	renderCell = (item,key,hasError) => {
		const {curCell} = this.props;

		if(curCell.x < 0){
			return (<GridCell
				key={key}
				style={[styles.cell,rowStyle[item.y],colStyle[item.x]]}
				item={item}
				size={gridCellW}
				onChangeState={this.onChangeCellState}
				active={0}
			  error={hasError}
			/>)
		}

		if(curCell.isStatic || (!curCell.isMark && curCell.data > 0)){
			return (<GridCell
				key={key}
				style={[styles.cell,rowStyle[item.y],colStyle[item.x]]}
				item={item}
				size={gridCellW}
				onChangeState={this.onChangeCellState}
			  active={item.data == curCell.data ? 2 : 0}
				error={hasError}
			/>)
		} else if(curCell.x === item.x && curCell.y === item.y){
			return (<GridCell
				key={key}
				style={[styles.cell,rowStyle[item.y],colStyle[item.x]]}
				item={curCell}
				size={gridCellW}
				onChangeState={this.onChangeCellState}
				active={2}
				error={hasError}
			/>)
		} else if(curCell.x === item.x ||
			curCell.y === item.y ||
			parseInt(curCell.x/3) === parseInt(item.x/3) &&
			parseInt(curCell.y/3) === parseInt(item.y/3)){
			return (<GridCell
				key={key}
				style={[styles.cell,rowStyle[item.y],colStyle[item.x]]}
				item={item}
				size={gridCellW}
				onChangeState={this.onChangeCellState}
				active={1}
				error={hasError}
			/>)
		} else {
			return (<GridCell
				key={key}
				style={[styles.cell,rowStyle[item.y],colStyle[item.x]]}
				item={item}
				size={gridCellW}
				onChangeState={this.onChangeCellState}
				active={0}
				error={hasError}
			/>)
		}

	};

	render(){
		const {cells,errorMap} = this.props;

		return (
			<View style={styles.grid}>
				{
					Array(9).fill().map((v,indexRow) => {
						return (
							<View key={indexRow} style={styles.row}>
								{
									cells.slice(indexRow*9,indexRow*9+9).map((item,indexCol) => {
										return this.renderCell(item,indexCol,errorMap[indexRow*9+indexCol]);
									})
								}
						</View>);
					})
				}
			</View>
		);
	}
}

GridLayout.propTypes = {

};

const rowStyle = [
	null,
	null,
	{borderBottomWidth:1,borderBottomColor:'black'},
	{borderTopWidth:1,borderTopColor:'black',marginTop:1},
	null,
	{borderBottomWidth:1,borderBottomColor:'black'},
	{borderTopWidth:1,borderTopColor:'black',marginTop:1},
	null,
	{borderBottomWidth:1,borderBottomColor:'#cfcfcf'},
	];

const colStyle = [
	null,
	null,
	{borderRightWidth:1,borderRightColor:'black'},
	{borderLeftWidth:1,borderLeftColor:'black',marginLeft:1},
	null,
	{borderRightWidth:1,borderRightColor:'black'},
	{borderLeftWidth:1,borderLeftColor:'black',marginLeft:1},
	null,
	{borderRightWidth:1,borderRightColor:'#cfcfcf'},
];

const styles = StyleSheet.create({
	grid:{
		marginTop:10,
		backgroundColor:'#fff',
		marginLeft:gridMarginLeft,
		marginRight:gridMarginLeft,
	},
	row:{
		height:gridCellW,
		flexDirection:'row',
	},
	cell:{
		width:gridCellW,
		borderColor:'#cfcfcf',
		borderLeftWidth:1,
		borderTopWidth:1,
		alignItems:'center',
		justifyContent:'center',
	},

});

const mapStateToProps = (state) => {
	return {
		curCell:state.curCell,
		cells:state.cells,
		errorMap:state.errorMap,
	};
};



const mapDispatchToProps = {
	setCurCell:(cell) => {
		return {
			type:ACTION_SET_CUR_CELL,
			payload:{
				cell
			}
		}
	},
	updateCells:(cell) => {
		return {
			type:ACTION_UPDATE_CELLS,
			payload:{
				x:cell.x,
				y:cell.y,
				cell,
			}
		}
	},
	addMemory:addMemory,
	changeCellSelOfKeyboard:(curItem) => {
		return changeCellSel(curItem);
	}
};

export default connect(mapStateToProps,mapDispatchToProps)(GridLayout);