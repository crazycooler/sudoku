/**
 * Created by coder on 2017/12/26.
 */
import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {View,Text,StyleSheet,TouchableWithoutFeedback} from 'react-native'
import {keyboardH,keyboardW5,circleSize} from './common/CommonSize'

export default class KeyboardNumButton extends Component{
	constructor(props){
		super(props);
	}

	render(){
		const {buttonPress,value,numEnable,numMark} = this.props;

		return (
			<TouchableWithoutFeedback
				onPress={() => buttonPress(value)}>
				<View
					style={[styles.buttonView,styles.numButtonView]}>
					<Text
						style={[styles.numButtonText,numEnable ? styles.normalButtonTextColor : styles.disableButtonTextColor]}>
						{value}
					</Text>
					<View style={[styles.circleView,numMark ? styles.hotCircleColor : styles.disableCircleColor]}/>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

KeyboardNumButton.propTypes = {
	buttonPress:PropTypes.func,
	value:PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]) ,
	numEnable:PropTypes.bool,
	numMark:PropTypes.bool,
};




const styles = StyleSheet.create({
	buttonView:{
		borderTopWidth:1,
		borderLeftWidth:1,
		borderColor:'#9b9ed1',
		height:keyboardH,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#594e75',
		flexDirection:'row',
	},
	numButtonView:{
		width:keyboardW5,
	},
	numButtonText:{
		fontSize:keyboardH*0.6,
	},
	normalButtonTextColor:{
		color:'#d2e0ea',
	},
	disableButtonTextColor:{
		color:'#777',
	},
	hotButtonTextColor:{
		color:'#ff9c1a',
	},
	disableCircleColor:{
		backgroundColor:'#777',
	},
	hotCircleColor:{
		backgroundColor:'#ff9c1a',
	},
	circleView:{
		width:circleSize,
		height:circleSize,
		borderRadius:circleSize/2,
		position:'absolute',
		top:circleSize,
		right:circleSize,
	}
});