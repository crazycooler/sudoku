/**
 * Created by coder on 2018/1/11.
 */
import sudoData from './data'
import {indexOfClass} from '../common/data'

export default (level,classIndex) => {
	const className = indexOfClass[classIndex];
	return sudoData[className][level];
}