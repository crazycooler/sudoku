/**
 * Created by crazycooler on 2017/12/23.
 */
import React,{Component} from 'react'
import {Text,View,StyleSheet} from 'react-native'
import GridLayout from './GridLayout'
import Keybord from './Keyboard'
import {connect} from 'react-redux'
import {ACTION_KEYBOARD_RESET,
	ACTION_CUR_CELL_RESET,
	ACTION_CELLS_RESET,
	ACTION_ERROR_MAP_RESET,
	ACTION_SET_CUR_LEVEL,
	ACTION_SET_CELLS} from './store/action'
import getSudoData from './data'
import {indexOfClass,indexOfTitle} from './common/data'
import {addMemory} from './common/memoryAction'


class Main extends Component{
	//导航条
	static navigationOptions = ({navigation}) => {
		//level 关卡数  classIndex  难度类型
		const {level,classIndex} = navigation.state.params;

		const title = indexOfTitle[classIndex];
		return {
			headerTitle:title + `--第${level+1}关`,
			headerStyle:{
				height:50,
			},
		};
	};

	constructor(props){
		super(props);

	}

	componentDidMount() {
		const {classIndex,level} = this.props.navigation.state.params;
		const {curLevel} = this.props;
		if(classIndex !== curLevel.classIndex || level !== curLevel.level){
			//如果不是当前关卡，则重新加载数据
			this.props.resetKeyboard();
			this.props.resetCurCell();
			this.props.resetErrorMap();
			this.props.setCurLevel({classIndex, level});

			const curSudoData = getSudoData(level,classIndex);
			this.props.setCells(curSudoData);
			this.props.addMemory();
		}
	}

	render(){
		const {showKeyboard,navigation} = this.props;

		return (
			<View style={styles.mainView}>
				<GridLayout/>
				{showKeyboard ?
					<Keybord
						navigation={navigation}
						style={styles.keyboardView}/>
					: null}
			</View>
		);

	}
}

const styles = StyleSheet.create({
	mainView:{
		height:'100%',
		backgroundColor:'#ddd',
	},
	keyboardView:{
		position:'absolute',
		bottom:0,
	},
});

const mapStateToProps = (state) => {
	return {
		showKeyboard : state.keyboard.show,
		curLevel:state.level.curLevel,
	}
};

const mapDispatchToProps = {
	resetKeyboard:() => {
		return {type:ACTION_KEYBOARD_RESET};
	},
	resetCurCell: () => {
		return {type:ACTION_CUR_CELL_RESET};
	},
	resetErrorMap:() => {
		return {type:ACTION_ERROR_MAP_RESET};
	},
	setCurLevel:(curLevel) => {
		return {
			type:ACTION_SET_CUR_LEVEL,
			payload:{curLevel}
		}
	},
	setCells:(curSudoData) => {
		return {
			type:ACTION_SET_CELLS,
			payload:{
				data:curSudoData,
			}
		}
	},
	addMemory:addMemory,
};

export default connect(mapStateToProps,mapDispatchToProps)(Main);