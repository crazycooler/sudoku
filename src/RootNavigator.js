/**
 * Created by coder on 2018/1/2.
 */
import { StackNavigator } from 'react-navigation'
import Home from './Home'
import LevelListPage from './LevelListPage'
import Main from './Main'



const RootNavigator = StackNavigator({
	Home:{
		screen:Home,
		navigationOptions:{
			headerTitle:'数独',
			headerStyle:{
				height:50,
			},
		},
	},
	LevelListPage:{
		screen:LevelListPage,
	},
	Main:{
		screen:Main,
	}
},{

});


export default RootNavigator;