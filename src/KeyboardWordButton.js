/**
 * Created by coder on 2017/12/26.
 */
import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {View,Text,TouchableWithoutFeedback,StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {keyboardH} from './common/CommonSize'

export default class KeyboardWordButton extends Component{
	constructor(props){
		super(props);
	}

	defaultBtnPress = () => {

	};

	render(){
		const {buttonPress,iconName,value,width,name,enable,hot} = this.props;
		let color = enable ? '#d2e0ea' : '#777';
		color = hot ? '#ff9c1a' : color;

		const btnPressEvent = enable ? buttonPress : this.defaultBtnPress;

		return(
			<TouchableWithoutFeedback
				onPress={() => btnPressEvent(name)}>
				<View
					style={[styles.buttonView,{width}]}>
					<Icon name={iconName} color={color} size={keyboardH*0.4} />
					<Text
						style={[styles.wordButtonText,{color}]}>
						{value}
					</Text>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

KeyboardWordButton.propTypes = {
	buttonPress:PropTypes.func,
	iconName:PropTypes.string,
	name:PropTypes.string,
	value:PropTypes.string,
	width:PropTypes.number,
	enable:PropTypes.bool,
	hot:PropTypes.bool,
};

const styles = StyleSheet.create({
	buttonView:{
		borderTopWidth:1,
		borderLeftWidth:1,
		borderColor:'#9b9ed1',
		height:keyboardH,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#594e75',
		flexDirection:'row',
	},
	wordButtonText:{
		fontSize:keyboardH*0.4,
		marginLeft:6,
	}
});