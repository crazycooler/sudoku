/**
 * Created by coder on 2018/1/2.
 */
import React,{Component} from 'react'
import {View,Text,TouchableNativeFeedback,StyleSheet} from 'react-native'


class ChoiceButton extends Component{
	render(){
		const {title,style,onPress} = this.props;
		return (
			<TouchableNativeFeedback
				onPress={() => onPress()}
			>
				<View
					style={[style,styles.choiceButton]}
				>
					<Text>{title}</Text>
				</View>
			</TouchableNativeFeedback>
		);
	}
}


class Home extends Component{
	constructor(props){
		super(props);
	}

	render(){

		const {navigate} = this.props.navigation;

		return(
			<View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
				<ChoiceButton
					style={styles.choiceButton}
					onPress={() => {navigate('LevelListPage',{classIndex:0})}}
				  title="入门"
				/>
				<ChoiceButton
					style={styles.choiceButton}
					onPress={() => {navigate('LevelListPage',{classIndex:1})}}
				  title="初级"
				/>
				<ChoiceButton
					style={styles.choiceButton}
					onPress={() => {navigate('LevelListPage',{classIndex:2})}}
				  title="中级"
				/>
				<ChoiceButton
					style={styles.choiceButton}
					onPress={() => {navigate('LevelListPage',{classIndex:3})}}
				  title="高级"
				/>
				<ChoiceButton
					style={styles.choiceButton}
					onPress={() => {navigate('LevelListPage',{classIndex:4})}}
				  title="骨灰级"
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	choiceButton:{
		marginBottom:30,
		borderWidth:1,
		borderColor:'#afc1ff',
		width:'60%',
		alignItems:'center',
		padding:8,
	}
});

export default Home;